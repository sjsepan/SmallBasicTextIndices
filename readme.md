# TextWindow (Console) app sample, on Windows, in SmallBasic v1.2

## About

SmallBasic examples of using text indices in arrays

![SmallBasicTextIndices.png](./SmallBasicTextIndices.png?raw=true)
![SmallBasicTextIndices2.png](./SmallBasicTextIndices2.png?raw=true)

Steve Sepan
sjsepan@yahoo.com
5-14-2023
